import { Dialog } from '@headlessui/react';
import { useRef, useState } from 'react';

import { SignInButton } from '~/components/domain/auth/SignInButton';
import { Head } from '~/components/shared/Head';
import liff from '@line/liff';
import styles from '~/components/liff/liff.module.css';
import Header from '~/components/liff/Header';
import Snippet from '~/components/liff/Snippet';
import Input from '~/components/liff/Input';

function Index() {
  const [isOpen, setIsOpen] = useState(true);
  const completeButtonRef = useRef(null);

  let isLoggedIn = false;
  try {
    isLoggedIn = liff.isLoggedIn();
  } catch (e) {
    console.log(e);
  }

  return (
    <>
      <Header />
      <div className={styles.container}>
        <div className={styles.liffIdBox}>
          <Input label="CURRENT LIFF ID" readonly value={'1656508316-k7jNojXm' || ''} />
        </div>
        <h1>Client APIs</h1>
        {!isLoggedIn ? (
          <Snippet
            apiName="liff.login()"
            version="2.0"
            docUrl="https://developers.line.biz/en/reference/liff/#login"
            skipAutoRun
            runner={async () => {
              return liff.login();
            }}
          />
        ) : (
          <Snippet
            apiName="liff.logout()"
            version="2.0"
            docUrl="https://developers.line.biz/en/reference/liff/#logout"
            skipAutoRun
            hideResponse
            runner={async () => {
              // reload after logout.
              setTimeout(() => {
                location.reload();
              }, 1000);
              return liff.logout();
            }}
          />
        )}
        <Snippet
          apiName="liff.getOS()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#get-os"
          runner={async () => {
            return liff.getOS();
          }}
        />
        <Snippet
          apiName="liff.getLanguage()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#get-language"
          runner={async () => {
            return liff.getLanguage();
          }}
        />
        <Snippet
          apiName="liff.getVersion()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#get-version"
          runner={async () => {
            return liff.getVersion();
          }}
        />
        <Snippet
          apiName="liff.getLineVersion()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#get-line-version"
          runner={async () => {
            return liff.getLineVersion();
          }}
        />
        <Snippet
          apiName="liff.isInClient()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#is-in-client"
          runner={async () => {
            return liff.isInClient();
          }}
        />
        <Snippet
          apiName="liff.isLoggedIn()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#is-logged-in"
          runner={async () => {
            return liff.isLoggedIn();
          }}
        />
        <Snippet
          apiName="liff.isApiAvailable()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#is-api-available"
          needRequestPayload
          defaultRequestPayload={'shareTargetPicker'}
          runner={async (api) => {
            return liff.isApiAvailable(api);
          }}
        />
        <Snippet
          apiName="liff.getAccessToken()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#get-access-token"
          runner={async () => {
            return liff.getAccessToken();
          }}
        />
        <Snippet
          apiName="liff.getIDToken()"
          version="2.1"
          docUrl="https://developers.line.biz/en/reference/liff/#get-id-token"
          runner={async () => {
            return liff.getIDToken();
          }}
        />
        <Snippet
          apiName="liff.getDecodedIDToken()"
          version="2.0"
          useTextareaForResponse
          docUrl="https://developers.line.biz/en/reference/liff/#get-decoded-id-token"
          runner={async () => {
            return JSON.stringify(await liff.getDecodedIDToken(), null, 4);
          }}
        />
        <Snippet
          apiName="liff.getContext()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#get-context"
          useTextareaForResponse
          runner={async () => {
            return JSON.stringify(await liff.getContext(), null, 4);
          }}
        />
        <Snippet
          apiName="liff.getProfile()"
          version="1.0"
          docUrl="https://developers.line.biz/en/reference/liff/#get-profile"
          useTextareaForResponse
          runner={async () => {
            return JSON.stringify(await liff.getProfile(), null, 4);
          }}
        />
        <Snippet
          apiName="liff.getFriendship()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#get-friendship"
          useTextareaForResponse
          runner={async () => {
            return JSON.stringify(await liff.getFriendship(), null, 4);
          }}
        />
        <Snippet
          apiName="liff.permanentLink.setExtraQueryParam()"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#permanent-linke-set-extra-query-param"
          needRequestPayload
          defaultRequestPayload={'user_tracking_id=8888'}
          runner={async (params) => {
            liff.permanentLink.setExtraQueryParam(params);
            return liff.permanentLink.createUrl();
          }}
        />
        <Snippet
          apiName="liff.sendMessages"
          version="1.0"
          docUrl="https://developers.line.biz/en/reference/liff/#send-messages"
          needRequestPayload
          hideResponse
          defaultRequestPayload={JSON.stringify(
            [
              {
                type: 'text',
                text: 'Hello, World!',
              },
            ],
            null,
            4
          )}
          skipAutoRun
          runner={async (messages) => {
            return await liff.sendMessages(JSON.parse(messages));
          }}
        />
        <Snippet
          apiName="liff.openWindow"
          version="1.0"
          docUrl="https://developers.line.biz/en/reference/liff/#open-window"
          needRequestPayload
          defaultRequestPayload={JSON.stringify(
            {
              url: 'https://line.me',
              external: true,
            },
            null,
            4
          )}
          skipAutoRun
          hideResponse
          runner={async (options) => {
            return await liff.openWindow(JSON.parse(options));
          }}
        />
        <Snippet
          apiName="liff.shareTargetPicker"
          version="2.0"
          docUrl="https://developers.line.biz/en/reference/liff/#share-target-picker"
          needRequestPayload
          hideResponse
          defaultRequestPayload={JSON.stringify(
            [
              {
                type: 'text',
                text: 'Hello, World!',
              },
            ],
            null,
            4
          )}
          skipAutoRun
          runner={async (options) => {
            return await liff.shareTargetPicker(JSON.parse(options));
          }}
        />
        <Snippet
          apiName="liff.scanCodeV2"
          version="2.15"
          docUrl="https://developers.line.biz/en/reference/liff/#scan-code-v2"
          skipAutoRun
          runner={async () => {
            if (liff.scanCodeV2) {
              return await liff.scanCodeV2();
            } else {
              return 'scanCode API is not available on this platform';
            }
          }}
        />
        <Snippet
          apiName="liff.closeWindow"
          version="1.0"
          docUrl="https://developers.line.biz/en/reference/liff/#close-window"
          skipAutoRun
          hideResponse
          runner={async () => {
            return await liff.closeWindow();
          }}
        />
      </div>
      <Head title="TOP PAGE" />
      <div className="bg-gray-100">{/* <Auth /> */}</div>
      <div className="hero min-h-screen">
        <div className="text-center hero-content">
          <div>
            <h1 className="text-3xl font-bold">TEST</h1>
            <p className="mt-4 text-lg">
              For fast <b>prototyping</b>. Already set up{' '}
              <a
                className="link link-primary"
                target="_blank"
                href="https://github.com/firebase/firebase-js-sdk"
                rel="noreferrer"
              >
                Firebase(v9)
              </a>
              ,{' '}
              <a className="link link-primary" target="_blank" href="https://daisyui.com/" rel="noreferrer">
                daisyUI
              </a>
              ,{' '}
              <a className="link link-primary" target="_blank" href="https://github.com/eslint/eslint" rel="noreferrer">
                ESLint
              </a>
              ,{' '}
              <a
                className="link link-primary"
                target="_blank"
                href="https://github.com/prettier/prettier"
                rel="noreferrer"
              >
                Prettier
              </a>
              .
            </p>
            <div className="mt-4 grid gap-2">
              <SignInButton />
              <button onClick={() => setIsOpen(true)}>Display Dialog</button>
            </div>
          </div>
        </div>
      </div>
      <Dialog
        className="flex fixed inset-0 z-10 overflow-y-auto"
        initialFocus={completeButtonRef}
        open={isOpen}
        onClose={() => setIsOpen(false)}
      >
        <div className="flex items-center justify-center min-h-screen w-screen">
          <Dialog.Overlay className="fixed inset-0 bg-black opacity-30" />
          <div className="relative bg-white rounded max-w-120 p-8 mx-auto">
            <Dialog.Title>Dialog Title</Dialog.Title>
            <Dialog.Description>Dialog description</Dialog.Description>
            <button
              ref={completeButtonRef}
              type="button"
              className="inline-flex justify-center px-4 py-2 text-sm font-medium text-blue-900 bg-blue-100 border border-transparent rounded-md hover:bg-blue-200 focus:outline-none focus-visible:ring-2 focus-visible:ring-offset-2 focus-visible:ring-blue-500"
              onClick={() => setIsOpen(false)}
            >
              Got it, thanks!
            </button>
          </div>
        </div>
      </Dialog>
    </>
  );
}

export default Index;
