import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import { App } from '~/components/root/App';
import liff from '@line/liff';

liff
  .init({ liffId: '1656508316-k7jNojXm' || '' })
  .then(() => {
    ReactDOM.render(
      <React.StrictMode>
        <App />
      </React.StrictMode>,
      document.getElementById('root')
    );
  })
  .catch((e) => {
    alert(`LIFF error: ${e.message}`);
  });
